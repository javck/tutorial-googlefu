﻿using UnityEngine;
using System.Collections;
using GoogleFu;

public class GoogleFu_Example : MonoBehaviour {
	//Example of an Object based Database
	SampleCharacters _charDb;
	//Example of an Static based Database
	SampleWeapons _weaponObj;



	// Use this for initialization
	void Start () {
		//GameObject Database Data Retrieval
		GameObject charObj = GameObject.Find("databaseObj");
		if(charObj != null){
			_charDb = charObj.GetComponent<SampleCharacters>();

			//Access The Row Method 1
			SampleCharactersRow row1 = _charDb.Rows[(int)SampleCharacters.rowIds.AI_TROLL];
			Debug.Log("row1:" + row1.ToString());

			//Access The Row Method 2
			SampleCharactersRow row2 = _charDb.GetRow(SampleCharacters.rowIds.AI_ORC);
			Debug.Log("row2:" +row2.ToString());

			//Access The Row Method3
			SampleCharactersRow row3 = _charDb.GetRow("AI_ORC");
			Debug.Log("row3:" + row3.ToString());

			//Access Random Row
			SampleCharactersRow row4 = _charDb.Rows[Random.Range(0,_charDb.Rows.Count)];
			Debug.Log("row4:" + row4.ToString());

			//Get the column's row data
			Debug.Log("row1's Name : " + row1._NAME);
		}

		//Static Database Data Retrieval
		//The SampleWeapons database is a Static class. Use it by grabbing the .Instance, this will
		//ensure the database is correctly initialized. Larger databases may take a while to initialize,
		//so grabbing an instance before the game is updating is recommended.
		_weaponObj = SampleWeapons.Instance;

		//Accessing The Row and getting  the data is the same as gameobject database's.
		SampleWeaponsRow sword = _weaponObj.GetRow (SampleWeapons.rowIds.WP_SWORD);
		Debug.Log ("sword: " + sword.ToString ());
		Debug.Log ("sword's Damage: " + sword._DAMAGE);

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
