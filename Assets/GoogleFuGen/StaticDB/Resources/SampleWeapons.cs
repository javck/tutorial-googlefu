//----------------------------------------------
//    GoogleFu: Google Doc Unity integration
//         Copyright ?? 2013 Litteratus
//
//        This file has been auto-generated
//              Do not manually edit
//----------------------------------------------

using UnityEngine;

namespace GoogleFu
{
	[System.Serializable]
	public class SampleWeaponsRow 
	{
		public string _NAME;
		public float _DAMAGE;
		public float _SPEED;
		public float _COOLDOWN;
		public float _ACCURACYINDEGREES;
		public SampleWeaponsRow(string __NAME, string __DAMAGE, string __SPEED, string __COOLDOWN, string __ACCURACYINDEGREES) 
		{
			_NAME = __NAME;
			{
			float res;
				if(float.TryParse(__DAMAGE, out res))
					_DAMAGE = res;
				else
					Debug.LogError("Failed To Convert DAMAGE string: "+ __DAMAGE +" to float");
			}
			{
			float res;
				if(float.TryParse(__SPEED, out res))
					_SPEED = res;
				else
					Debug.LogError("Failed To Convert SPEED string: "+ __SPEED +" to float");
			}
			{
			float res;
				if(float.TryParse(__COOLDOWN, out res))
					_COOLDOWN = res;
				else
					Debug.LogError("Failed To Convert COOLDOWN string: "+ __COOLDOWN +" to float");
			}
			{
			float res;
				if(float.TryParse(__ACCURACYINDEGREES, out res))
					_ACCURACYINDEGREES = res;
				else
					Debug.LogError("Failed To Convert ACCURACYINDEGREES string: "+ __ACCURACYINDEGREES +" to float");
			}
		}

		public string GetStringData( string colID )
		{
			string ret = System.String.Empty;
			switch( colID.ToUpper() )
			{
				case "NAME":
					ret = _NAME.ToString();
					break;
				case "DAMAGE":
					ret = _DAMAGE.ToString();
					break;
				case "SPEED":
					ret = _SPEED.ToString();
					break;
				case "COOLDOWN":
					ret = _COOLDOWN.ToString();
					break;
				case "ACCURACYINDEGREES":
					ret = _ACCURACYINDEGREES.ToString();
					break;
			}

			return ret;
		}
		public override string ToString()
		{
			string ret = System.String.Empty;
			ret += "{" + "NAME" + " : " + _NAME.ToString() + "} ";
			ret += "{" + "DAMAGE" + " : " + _DAMAGE.ToString() + "} ";
			ret += "{" + "SPEED" + " : " + _SPEED.ToString() + "} ";
			ret += "{" + "COOLDOWN" + " : " + _COOLDOWN.ToString() + "} ";
			ret += "{" + "ACCURACYINDEGREES" + " : " + _ACCURACYINDEGREES.ToString() + "} ";
			return ret;
		}
	}
	public sealed class SampleWeapons
	{
		public enum rowIds {
			WP_ROCK, WP_STICK, WP_SPEAR, WP_SWORD, WP_FIRE_BREATH
		};
		public string [] rowNames = {
			"WP_ROCK", "WP_STICK", "WP_SPEAR", "WP_SWORD", "WP_FIRE_BREATH"
		};
		public System.Collections.Generic.List<SampleWeaponsRow> Rows = new System.Collections.Generic.List<SampleWeaponsRow>();

		public static SampleWeapons Instance
		{
			get { return NestedSampleWeapons.instance; }
		}

		private class NestedSampleWeapons
		{
			static NestedSampleWeapons() { }
			internal static readonly SampleWeapons instance = new SampleWeapons();
		}

		private SampleWeapons()
		{
			Rows.Add( new SampleWeaponsRow("Rock",
														"1",
														"1",
														"0.5",
														"120"));
			Rows.Add( new SampleWeaponsRow("Stick",
														"1.1",
														"1",
														"0.45",
														"50"));
			Rows.Add( new SampleWeaponsRow("Spear",
														"1.2",
														"1.5",
														"0.6",
														"30"));
			Rows.Add( new SampleWeaponsRow("Sword",
														"2.5",
														"1.5",
														"0.8",
														"10"));
			Rows.Add( new SampleWeaponsRow("Fire Breath",
														"5.5",
														"0.6",
														"1.2",
														"180"));
		}
		public SampleWeaponsRow GetRow(rowIds rowID)
		{
			SampleWeaponsRow ret = null;
			try
			{
				ret = Rows[(int)rowID];
			}
			catch( System.Collections.Generic.KeyNotFoundException ex )
			{
				Debug.LogError( rowID + " not found: " + ex.Message );
			}
			return ret;
		}
		public SampleWeaponsRow GetRow(string rowString)
		{
			SampleWeaponsRow ret = null;
			try
			{
				ret = Rows[(int)System.Enum.Parse(typeof(rowIds), rowString)];
			}
			catch(System.ArgumentException) {
				Debug.LogError( rowString + " is not a member of the rowIds enumeration.");
			}
			return ret;
		}

	}

}
