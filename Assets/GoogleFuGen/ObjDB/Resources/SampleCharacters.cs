//----------------------------------------------
//    GoogleFu: Google Doc Unity integration
//         Copyright ?? 2013 Litteratus
//
//        This file has been auto-generated
//              Do not manually edit
//----------------------------------------------

using UnityEngine;

namespace GoogleFu
{
	[System.Serializable]
	public class SampleCharactersRow 
	{
		public string _NAME;
		public int _LEVEL;
		public bool _CANFLY;
		public string _WEAPON;
		public float _BASEMODIFIER;
		public string _ARCHETYPE;
		public int _STRENGTH;
		public int _ENDURANCE;
		public int _INTELLIGENCE;
		public int _DEXTERITY;
		public int _HEALTH;
		public int _MANA;
		public int _SPEED;
		public Color _BASECOLOR;
		public Vector3 _OFFSET;
		public SampleCharactersRow(string __NAME, string __LEVEL, string __CANFLY, string __WEAPON, string __BASEMODIFIER, string __ARCHETYPE, string __STRENGTH, string __ENDURANCE, string __INTELLIGENCE, string __DEXTERITY, string __HEALTH, string __MANA, string __SPEED, string __BASECOLOR, string __OFFSET) 
		{
			_NAME = __NAME;
			{
			int res;
				if(int.TryParse(__LEVEL, out res))
					_LEVEL = res;
				else
					Debug.LogError("Failed To Convert LEVEL string: "+ __LEVEL +" to int");
			}
			{
			bool res;
				if(bool.TryParse(__CANFLY, out res))
					_CANFLY = res;
				else
					Debug.LogError("Failed To Convert CANFLY string: "+ __CANFLY +" to bool");
			}
			_WEAPON = __WEAPON;
			{
			float res;
				if(float.TryParse(__BASEMODIFIER, out res))
					_BASEMODIFIER = res;
				else
					Debug.LogError("Failed To Convert BASEMODIFIER string: "+ __BASEMODIFIER +" to float");
			}
			_ARCHETYPE = __ARCHETYPE;
			{
			int res;
				if(int.TryParse(__STRENGTH, out res))
					_STRENGTH = res;
				else
					Debug.LogError("Failed To Convert STRENGTH string: "+ __STRENGTH +" to int");
			}
			{
			int res;
				if(int.TryParse(__ENDURANCE, out res))
					_ENDURANCE = res;
				else
					Debug.LogError("Failed To Convert ENDURANCE string: "+ __ENDURANCE +" to int");
			}
			{
			int res;
				if(int.TryParse(__INTELLIGENCE, out res))
					_INTELLIGENCE = res;
				else
					Debug.LogError("Failed To Convert INTELLIGENCE string: "+ __INTELLIGENCE +" to int");
			}
			{
			int res;
				if(int.TryParse(__DEXTERITY, out res))
					_DEXTERITY = res;
				else
					Debug.LogError("Failed To Convert DEXTERITY string: "+ __DEXTERITY +" to int");
			}
			{
			int res;
				if(int.TryParse(__HEALTH, out res))
					_HEALTH = res;
				else
					Debug.LogError("Failed To Convert HEALTH string: "+ __HEALTH +" to int");
			}
			{
			int res;
				if(int.TryParse(__MANA, out res))
					_MANA = res;
				else
					Debug.LogError("Failed To Convert MANA string: "+ __MANA +" to int");
			}
			{
			int res;
				if(int.TryParse(__SPEED, out res))
					_SPEED = res;
				else
					Debug.LogError("Failed To Convert SPEED string: "+ __SPEED +" to int");
			}
			{
				string [] splitpath = __BASECOLOR.Split(new char[]{',', ' '},System.StringSplitOptions.RemoveEmptyEntries);
				if(splitpath.Length != 3 && splitpath.Length != 4)
					Debug.LogError("Incorrect number of paramaters for Color in " + __BASECOLOR );
				float []results = new float[splitpath.Length];
				for(int i = 0; i < splitpath.Length; i++)
				{
					float res;
					if(float.TryParse(splitpath[i], out res))
					{
						results[i] = res;
					}
					else 
					{
						Debug.LogError("Error parsing " + __BASECOLOR + " Component: " + splitpath[i] + " parameter " + i + " of variable BASECOLOR");
					}
				}
				_BASECOLOR.r = results[0];
				_BASECOLOR.g = results[1];
				_BASECOLOR.b = results[2];
				if(splitpath.Length == 4)
					_BASECOLOR.a = results[3];
			}
			{
				string [] splitpath = __OFFSET.Split(new char[]{',', ' '},System.StringSplitOptions.RemoveEmptyEntries);
				if(splitpath.Length != 3)
					Debug.LogError("Incorrect number of paramaters for Vector3 in " + __OFFSET );
				float []results = new float[splitpath.Length];
				for(int i = 0; i < 3; i++)
				{
					float res;
					if(float.TryParse(splitpath[i], out res))
					{
						results[i] = res;
					}
					else 
					{
						Debug.LogError("Error parsing " + __OFFSET + " Component: " + splitpath[i] + " parameter " + i + " of variable OFFSET");
					}
				}
				_OFFSET.x = results[0];
				_OFFSET.y = results[1];
				_OFFSET.z = results[2];
			}
		}

		public string GetStringData( string colID )
		{
			string ret = System.String.Empty;
			switch( colID.ToUpper() )
			{
				case "NAME":
					ret = _NAME.ToString();
					break;
				case "LEVEL":
					ret = _LEVEL.ToString();
					break;
				case "CANFLY":
					ret = _CANFLY.ToString();
					break;
				case "WEAPON":
					ret = _WEAPON.ToString();
					break;
				case "BASEMODIFIER":
					ret = _BASEMODIFIER.ToString();
					break;
				case "ARCHETYPE":
					ret = _ARCHETYPE.ToString();
					break;
				case "STRENGTH":
					ret = _STRENGTH.ToString();
					break;
				case "ENDURANCE":
					ret = _ENDURANCE.ToString();
					break;
				case "INTELLIGENCE":
					ret = _INTELLIGENCE.ToString();
					break;
				case "DEXTERITY":
					ret = _DEXTERITY.ToString();
					break;
				case "HEALTH":
					ret = _HEALTH.ToString();
					break;
				case "MANA":
					ret = _MANA.ToString();
					break;
				case "SPEED":
					ret = _SPEED.ToString();
					break;
				case "BASECOLOR":
					ret = _BASECOLOR.ToString();
					break;
				case "OFFSET":
					ret = _OFFSET.ToString();
					break;
			}

			return ret;
		}
		public override string ToString()
		{
			string ret = System.String.Empty;
			ret += "{" + "NAME" + " : " + _NAME.ToString() + "} ";
			ret += "{" + "LEVEL" + " : " + _LEVEL.ToString() + "} ";
			ret += "{" + "CANFLY" + " : " + _CANFLY.ToString() + "} ";
			ret += "{" + "WEAPON" + " : " + _WEAPON.ToString() + "} ";
			ret += "{" + "BASEMODIFIER" + " : " + _BASEMODIFIER.ToString() + "} ";
			ret += "{" + "ARCHETYPE" + " : " + _ARCHETYPE.ToString() + "} ";
			ret += "{" + "STRENGTH" + " : " + _STRENGTH.ToString() + "} ";
			ret += "{" + "ENDURANCE" + " : " + _ENDURANCE.ToString() + "} ";
			ret += "{" + "INTELLIGENCE" + " : " + _INTELLIGENCE.ToString() + "} ";
			ret += "{" + "DEXTERITY" + " : " + _DEXTERITY.ToString() + "} ";
			ret += "{" + "HEALTH" + " : " + _HEALTH.ToString() + "} ";
			ret += "{" + "MANA" + " : " + _MANA.ToString() + "} ";
			ret += "{" + "SPEED" + " : " + _SPEED.ToString() + "} ";
			ret += "{" + "BASECOLOR" + " : " + _BASECOLOR.ToString() + "} ";
			ret += "{" + "OFFSET" + " : " + _OFFSET.ToString() + "} ";
			return ret;
		}
	}
	public class SampleCharacters :  GoogleFuComponentBase
	{
		public enum rowIds {
			AI_GOBLIN, AI_ORC, AI_TROLL, AI_DEATH_KNIGHT, AI_WYVERN
		};
		public string [] rowNames = {
			"AI_GOBLIN", "AI_ORC", "AI_TROLL", "AI_DEATH_KNIGHT", "AI_WYVERN"
		};
		public System.Collections.Generic.List<SampleCharactersRow> Rows = new System.Collections.Generic.List<SampleCharactersRow>();

		void Awake()
		{
			DontDestroyOnLoad(this);
		}
		public override void AddRowGeneric (System.Collections.Generic.List<string> input)
		{
			Rows.Add(new SampleCharactersRow(input[0],input[1],input[2],input[3],input[4],input[5],input[6],input[7],input[8],input[9],input[10],input[11],input[12],input[13],input[14]));
		}
		public override void Clear ()
		{
			Rows.Clear();
		}
		public SampleCharactersRow GetRow(rowIds rowID)
		{
			SampleCharactersRow ret = null;
			try
			{
				ret = Rows[(int)rowID];
			}
			catch( System.Collections.Generic.KeyNotFoundException ex )
			{
				Debug.LogError( rowID + " not found: " + ex.Message );
			}
			return ret;
		}
		public SampleCharactersRow GetRow(string rowString)
		{
			SampleCharactersRow ret = null;
			try
			{
				ret = Rows[(int)System.Enum.Parse(typeof(rowIds), rowString)];
			}
			catch(System.ArgumentException) {
				Debug.LogError( rowString + " is not a member of the rowIds enumeration.");
			}
			return ret;
		}

	}

}
